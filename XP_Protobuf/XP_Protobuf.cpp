
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"
#include "XPLMUtilities.h"
#include <iostream>
#include <string>
#include <string.h>
#if IBM
#include <windows.h>
#endif
#pragma comment(lib,"ws2_32.lib") //Winsock Library
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <tchar.h>

//Protobuf
#include "data.pb.h"

int iResult;
WSADATA wsaData;
char buffer[1024];
SOCKET dispatchSocket;
unsigned short Port = 8800, MCDUPort = 8410;
#define SERVER1 "192.168.71.36"
#define SERVER2 "192.168.71.35"
sockaddr_in  MCDU1, MCDU2, debugServer, pylonAddress;
int MCDU1_len = sizeof(MCDU1);
int MCDU2_len = sizeof(MCDU2);

int debugServer_len = sizeof(debugServer);

float OilTempOut[2], OilQuantityOut[2], FuelFlowOut[2];

//Custom dataref for debug mode
XPLMDataRef debugStatus = NULL;
int debugStatusValue;
int GetDebugStatus(void* inRefcon);
void SetDebugStatus(void* inRefcon, int outValue);
primaryData outProtobuf;
struct primaryDataOld {
	float Airspeed;
	float trueAirspeed;
	float SelectedAirspeed;
	float GroundSpeed;
	float Pitch;
	float Roll;
	float VerticalSpeed;
	float VerticalSpeedBug;
	float backupDisplayAltitude;
	float altitudeAboveGround;
	float SelectedAltitude;
	float Heading;
	float trueHeading;
	float SelectedHeading;
	float Latitude;
	float Longitude;
	float fuelLeftTank;
	float fuelRightTank;
	float fuelCenterTank;
	float N1Left;
	float N1Right;
	float N2Left;
	float N2Right;
	float EGTLeft;
	float EGTRight;
	float OilPressureLeft;
	float OilPressureRight;
	float OilTempLeft;
	float OilTempRight;
	float OilQuantityLeft;
	float OilQuantityRight;
	float FlapsHandle;
	float WindDirection;
	float Windspeed;
	int FlightDirectorStatus;
	float FlightDirectorPitch;
	float FlightDirectorRoll;
	float FuelFlowLeft;
	float FuelFlowRight;
	float GreenArcValue;
	int GreenArcStatus;
	float GreenArcDistance;
	float xtrack;
	float RadioAltimeter;
	int RadioAltimeterStep;
	float VNAV_ERR;
	int RunwayAltitude;
	int waypointAltitude[128];
	int zuluTime[3];
	int flapUpValue;
	int flapUpStatus;
	int flap1Value;
	int flap1Status;
	int flap5Value;
	int flap5Status;
	int flap15Value;
	int flap15Status;
	int flap25Value;
	int flap25Status;
	float maxSpeed;
	float minSpeed;
	int minSpeedStatus;
	int VORLineStatus;
	float VORLineCourse;
	float Course;
	int VOR1_Show;
	char VORName1[24];
	char VORName2[24];
	float VORDistance1;
	float VORDistance2;
	float runwayStart[2];
	float runwayEnd[2];
	float runwaycourse;
	float HdefPath;
	float NAVBearing;
	int DecisionHeight;
	int DecisionHeightStatus;
	int PullUpStatus;
	float Acceleration;
	float ILS_Horizontal_Value;
	float ILS_Vertical_Value;
	int ILS_Show;
	int ILS_Status;
	float VREFSpeed;
	int VREFStatus;
	int onGround;
	char scenario[5];
	float holdHeading1;
	float holdHeading2;
	float holdLegLength;
	float holdRadius;
	int holdStatus;
	float RNP;
	float ANP;
	float V1Speed;
	float V2Speed;
	float VRSpeed;
	float Mach;
	char Fix[5];
	int airspeedIsMach;
	int legs_alt_rest_type[128];
	float TOD;
	float throttleLevel1;
	float throttleLevel2;
	//Route data
	float latitudeArray[128];
	float longitudeArray[128];
	float latitudeArray2[128];
	float longitudeArray2[128];
	char waypointName[24];
	int legNumber;
	int legNumber2;
	float ETA;
	float waypointDistance;
	char legName[512];
	char legName2[512];
	char waypointNameWithType[512]; //List of waypoint that has to be display along with type
	int waypointType[20]; //Type of the waypoint and the route connect to it. 0 for none, 1 for non-active, 2 for active and 3 flyover, 4 for flyover active
	int legModActive; //Whether the route is being modified
	//Auto pilot data
	int autoThrottleStatus;
	int autoPilotSpeedMode;
	int autoPilotHeadingMode;
	int autopilotAltitudeMode;
	int autoPilotHeadingModeArm;
	int autoPilotAltitudeModeArm;
	int CMDAStatus;
	int CMDBStatus;
	//Data that needs to be either pilot or copilot
	//Pilot Side
	float AltitudePilot;
	int mapRangePilot;
	int airportStatusPilot;
	int waypointStatusPilot;
	int vor1StatusPilot;
	int vor2StatusPilot;
	int baroSTDStatusPilot;
	float baroInHgPilot;
	int baroUnitStatusPilot;
	int mapModePilot;
	int navaidStatusPilot;
	int pfdModePilot; //This is for determining whether it's ILS or LNAV/VNAV text
	int vertPathPilot; //This is for determining whether it's ILS or VOR
	char Text_Line1Pilot[10];
	char Text_Line2Pilot[10];
	int BaroBoxPilot;
	//Copilot Side
	float AltitudeCoPilot;
	int mapRangeCoPilot;
	int airportStatusCoPilot;
	int waypointStatusCoPilot;
	int vor1StatusCoPilot;
	int vor2StatusCoPilot;
	int baroSTDStatusCoPilot;
	float baroInHgCoPilot;
	int baroUnitStatusCoPilot;
	int mapModeCoPilot;
	int navaidStatusCoPilot;
	int pfdModeCoPilot;
	int vertPathCoPilot;
	char Text_Line1CoPilot[10];
	char Text_Line2CoPilot[10];
	int BaroBoxCoPilot;
};

/*struct routeData {
float latitudeArray[128];
float longitudeArray[128];
char waypointName[24];
int legNumber;
float ETA;
float Distance;
char legName[512];
};*/

struct MCDUData {
	char pageLabel_L[24];
	char pageLabel_S[24];
	char pageLabel_I[24];
	char line1_L[24];
	char line2_L[24];
	char line3_L[24];
	char line4_L[24];
	char line5_L[24];
	char line6_L[24];
	char line1_S[24];
	char line2_S[24];
	char line3_S[24];
	char line4_S[24];
	char line5_S[24];
	char line6_S[24];
	char line1_label_S[24];
	char line2_label_S[24];
	char line3_label_S[24];
	char line4_label_S[24];
	char line5_label_S[24];
	char line6_label_S[24];
	char line1_I[24];
	char line2_I[24];
	char line3_I[24];
	char line4_I[24];
	char line5_I[24];
	char line6_I[24];
	char entry_L[24];
	char entry_I[24];
	char line1_M[24];
	char line1_G[24];
	char line2_M[24];
	char line2_G[24];
	char line3_M[24];
	char line3_G[24];
	char line4_M[24];
	char line4_G[24];
	char line5_M[24];
	char line5_G[24];
	char line6_M[24];
	char line6_G[24];
	int page; //0 for MCDU, 1 for DataCom, 2 for AOC
};

primaryDataOld outData;
//routeData outRoute;
MCDUData outMCDU1;
MCDUData outMCDU2;

//Primary datarefs
XPLMDataRef Airspeed;
XPLMDataRef trueAirspeed;
XPLMDataRef SelectedAirspeed;
XPLMDataRef GroundSpeed;
XPLMDataRef Pitch;
XPLMDataRef Roll;
XPLMDataRef VerticalSpeed;
XPLMDataRef VerticalSpeedBug;
XPLMDataRef AltitudePilot;
XPLMDataRef AltitudeCoPilot;
XPLMDataRef backupDisplayAltitude;
XPLMDataRef altitudeAboveGround;
XPLMDataRef SelectedAltitude;
XPLMDataRef Heading;
XPLMDataRef trueHeading;
XPLMDataRef SelectedHeading;
XPLMDataRef Latitude;
XPLMDataRef Longitude;
XPLMDataRef fuelLeftTank;
XPLMDataRef fuelRightTank;
XPLMDataRef fuelCenterTank;
XPLMDataRef N1Left;
XPLMDataRef N1Right;
XPLMDataRef N2Left;
XPLMDataRef N2Right;
XPLMDataRef EGTLeft;
XPLMDataRef EGTRight;
XPLMDataRef OilPressureLeft;
XPLMDataRef OilPressureRight;
XPLMDataRef OilTemp;
XPLMDataRef OilQuantity;
XPLMDataRef FlapsHandle;
XPLMDataRef WindDirection;
XPLMDataRef Windspeed;
XPLMDataRef FlightDirectorStatus;
XPLMDataRef FlightDirectorPitch;
XPLMDataRef FlightDirectorRoll;
XPLMDataRef FuelFlow;
XPLMDataRef GreenArcStatus;
XPLMDataRef GreenArcValue;
XPLMDataRef GreenArcDistance;
XPLMDataRef xtrack;
XPLMDataRef RadioAltimeter;
XPLMDataRef RadioAltimeterStep;
XPLMDataRef VNAV_ERR;
XPLMDataRef RunwayAltitude;
XPLMDataRef waypointAltitude;
XPLMDataRef zuluTime_Hour;
XPLMDataRef zuluTime_Minute;
XPLMDataRef zuluTime_Second;
XPLMDataRef flapUpValue;
XPLMDataRef flapUpStatus;
XPLMDataRef flap1Value;
XPLMDataRef flap1Status;
XPLMDataRef flap5Value;
XPLMDataRef flap5Status;
XPLMDataRef flap15Value;
XPLMDataRef flap15Status;
XPLMDataRef flap25Value;
XPLMDataRef flap25Status;
XPLMDataRef maxSpeed;
XPLMDataRef minSpeed;
XPLMDataRef minSpeedStatus;
XPLMDataRef VORLineStatus;
XPLMDataRef VORLineCourse;
XPLMDataRef Course;
XPLMDataRef VOR1_Show;
XPLMDataRef VORName1;
XPLMDataRef VORName2;
XPLMDataRef VORDistance1;
XPLMDataRef VORDistance2;
XPLMDataRef runwayStartLat;
XPLMDataRef runwayStartLon;
XPLMDataRef runwayEndLat;
XPLMDataRef runwayEndLon;
XPLMDataRef runwaycourse;
XPLMDataRef HdefPath;
XPLMDataRef mapModePilot;
XPLMDataRef mapModeCoPilot;
XPLMDataRef NAVBearing;
XPLMDataRef DecisionHeight;
XPLMDataRef DecisionHeightStatus;
XPLMDataRef PullUpStatus;
XPLMDataRef Acceleration;
XPLMDataRef ILS_Horizontal_Value;
XPLMDataRef ILS_Vertical_Value;
XPLMDataRef ILS_Show;
XPLMDataRef ILS_Status;
XPLMDataRef VREFSpeed;
XPLMDataRef VREFStatus;
XPLMDataRef onGround;
XPLMDataRef scenario;
XPLMDataRef holdHeading1;
XPLMDataRef holdHeading2;
XPLMDataRef holdLegLength;
XPLMDataRef holdRadius;
XPLMDataRef holdStatus;
XPLMDataRef RNP;
XPLMDataRef ANP;
XPLMDataRef V1Speed;
XPLMDataRef V2Speed;
XPLMDataRef VRSpeed;
XPLMDataRef Mach;
XPLMDataRef Fix;
XPLMDataRef airspeedIsMach;
XPLMDataRef legs_alt_rest_type;
XPLMDataRef TOD;
XPLMDataRef throttleLevel1;
XPLMDataRef throttleLevel2;
//Route datarefs
XPLMDataRef legNumber;
XPLMDataRef waypointLatitudeArray;
XPLMDataRef waypointLongitudeArray;
XPLMDataRef legNumber2;
XPLMDataRef waypointLatitudeArray2;
XPLMDataRef waypointLongitudeArray2;
XPLMDataRef ETA;
XPLMDataRef waypointName;
XPLMDataRef waypointDistance;
XPLMDataRef legName;
XPLMDataRef legName2;
XPLMDataRef legModActive;
XPLMDataRef wptType_0;
XPLMDataRef wptType_1;
XPLMDataRef wptType_2;
XPLMDataRef wptType_3;
XPLMDataRef wptType_4;
XPLMDataRef wptType_5;
XPLMDataRef wptType_6;
XPLMDataRef wptType_7;
XPLMDataRef wptType_8;
XPLMDataRef wptType_9;
XPLMDataRef wptType_10;
XPLMDataRef wptType_11;
XPLMDataRef wptType_12;
XPLMDataRef wptType_13;
XPLMDataRef wptType_14;
XPLMDataRef wptType_15;
XPLMDataRef wptType_16;
XPLMDataRef wptType_17;
XPLMDataRef wptType_18;
XPLMDataRef wptType_19;
XPLMDataRef wptNameWhite_0;
XPLMDataRef wptNameWhite_1;
XPLMDataRef wptNameWhite_2;
XPLMDataRef wptNameWhite_3;
XPLMDataRef wptNameWhite_4;
XPLMDataRef wptNameWhite_5;
XPLMDataRef wptNameWhite_6;
XPLMDataRef wptNameWhite_7;
XPLMDataRef wptNameWhite_8;
XPLMDataRef wptNameWhite_9;
XPLMDataRef wptNameWhite_10;
XPLMDataRef wptNameWhite_11;
XPLMDataRef wptNameWhite_12;
XPLMDataRef wptNameWhite_13;
XPLMDataRef wptNameWhite_14;
XPLMDataRef wptNameWhite_15;
XPLMDataRef wptNameWhite_16;
XPLMDataRef wptNameWhite_17;
XPLMDataRef wptNameWhite_18;
XPLMDataRef wptNameWhite_19;
XPLMDataRef wptNameMagenta_0;
XPLMDataRef wptNameMagenta_1;
XPLMDataRef wptNameMagenta_2;
XPLMDataRef wptNameMagenta_3;
XPLMDataRef wptNameMagenta_4;
XPLMDataRef wptNameMagenta_5;
XPLMDataRef wptNameMagenta_6;
XPLMDataRef wptNameMagenta_7;
XPLMDataRef wptNameMagenta_8;
XPLMDataRef wptNameMagenta_9;
XPLMDataRef wptNameMagenta_10;
XPLMDataRef wptNameMagenta_11;
XPLMDataRef wptNameMagenta_12;
XPLMDataRef wptNameMagenta_13;
XPLMDataRef wptNameMagenta_14;
XPLMDataRef wptNameMagenta_15;
XPLMDataRef wptNameMagenta_16;
XPLMDataRef wptNameMagenta_17;
XPLMDataRef wptNameMagenta_18;
XPLMDataRef wptNameMagenta_19;

//Auto pilot datarefs
XPLMDataRef autoThrottleStatus;
XPLMDataRef autoPilotSpeedMode;
XPLMDataRef autoPilotHeadingMode;
XPLMDataRef autopilotAltitudeMode;
XPLMDataRef autoPilotHeadingModeArm;
XPLMDataRef autoPilotAltitudeModeArm;
XPLMDataRef CMDAStatus;
XPLMDataRef CMDBStatus;

XPLMDataRef pilotPage;
XPLMDataRef copilotPage;

//Data that needs to be either pilot or copilot
//Pilot Side
XPLMDataRef mapRangePilot;
XPLMDataRef airportStatusPilot;
XPLMDataRef waypointStatusPilot;
XPLMDataRef vor1StatusPilot;
XPLMDataRef vor2StatusPilot;
XPLMDataRef baroSTDStatusPilot;
XPLMDataRef baroInHgPilot;
XPLMDataRef baroUnitStatusPilot;
XPLMDataRef navaidStatusPilot;
XPLMDataRef pfdModePilot;
XPLMDataRef vertPathPilot;
XPLMDataRef Text_Line1Pilot;
XPLMDataRef Text_Line2Pilot;
XPLMDataRef BaroBoxPilot;
//CoPilot Side
XPLMDataRef mapRangeCoPilot;
XPLMDataRef airportStatusCoPilot;
XPLMDataRef waypointStatusCoPilot;
XPLMDataRef vor1StatusCoPilot;
XPLMDataRef vor2StatusCoPilot;
XPLMDataRef baroSTDStatusCoPilot;
XPLMDataRef baroInHgCoPilot;
XPLMDataRef baroUnitStatusCoPilot;
XPLMDataRef navaidStatusCoPilot;
XPLMDataRef pfdModeCoPilot;
XPLMDataRef vertPathCoPilot;
XPLMDataRef Text_Line1CoPilot;
XPLMDataRef Text_Line2CoPilot;
XPLMDataRef BaroBoxCoPilot;
//MCDU Dataref
XPLMDataRef pageLabel_L;
XPLMDataRef pageLabel_S;
XPLMDataRef pageLabel_I;
XPLMDataRef line1_L;
XPLMDataRef line2_L;
XPLMDataRef line3_L;
XPLMDataRef line4_L;
XPLMDataRef line5_L;
XPLMDataRef line6_L;
XPLMDataRef line1_S;
XPLMDataRef line2_S;
XPLMDataRef line3_S;
XPLMDataRef line4_S;
XPLMDataRef line5_S;
XPLMDataRef line6_S;
XPLMDataRef line1_label_S;
XPLMDataRef line2_label_S;
XPLMDataRef line3_label_S;
XPLMDataRef line4_label_S;
XPLMDataRef line5_label_S;
XPLMDataRef line6_label_S;
XPLMDataRef line1_I;
XPLMDataRef line2_I;
XPLMDataRef line3_I;
XPLMDataRef line4_I;
XPLMDataRef line5_I;
XPLMDataRef line6_I;
XPLMDataRef entry_L;
XPLMDataRef entry_I;
XPLMDataRef line1_M;
XPLMDataRef line2_M;
XPLMDataRef line3_M;
XPLMDataRef line4_M;
XPLMDataRef line5_M;
XPLMDataRef line6_M;
XPLMDataRef line1_G;
XPLMDataRef line2_G;
XPLMDataRef line3_G;
XPLMDataRef line4_G;
XPLMDataRef line5_G;
XPLMDataRef line6_G;

//CoPilot MCDU Dataref
XPLMDataRef pageLabel_L_;
XPLMDataRef pageLabel_S_;
XPLMDataRef pageLabel_I_;
XPLMDataRef line1_L_;
XPLMDataRef line2_L_;
XPLMDataRef line3_L_;
XPLMDataRef line4_L_;
XPLMDataRef line5_L_;
XPLMDataRef line6_L_;
XPLMDataRef line1_S_;
XPLMDataRef line2_S_;
XPLMDataRef line3_S_;
XPLMDataRef line4_S_;
XPLMDataRef line5_S_;
XPLMDataRef line6_S_;
XPLMDataRef line1_label_S_;
XPLMDataRef line2_label_S_;
XPLMDataRef line3_label_S_;
XPLMDataRef line4_label_S_;
XPLMDataRef line5_label_S_;
XPLMDataRef line6_label_S_;
XPLMDataRef line1_I_;
XPLMDataRef line2_I_;
XPLMDataRef line3_I_;
XPLMDataRef line4_I_;
XPLMDataRef line5_I_;
XPLMDataRef line6_I_;
XPLMDataRef entry_L_;
XPLMDataRef entry_I_;
XPLMDataRef line1_M_;
XPLMDataRef line2_M_;
XPLMDataRef line3_M_;
XPLMDataRef line4_M_;
XPLMDataRef line5_M_;
XPLMDataRef line6_M_;
XPLMDataRef line1_G_;
XPLMDataRef line2_G_;
XPLMDataRef line3_G_;
XPLMDataRef line4_G_;
XPLMDataRef line5_G_;
XPLMDataRef line6_G_;


//Setter and Getter of the custom dataref
int GetDebugStatus(void* inRefcon)
{
	return debugStatusValue;
}

void SetDebugStatus(void* inRefcon, int inValue)
{
	debugStatusValue = inValue;
}

void clearBuffer(char input[]) {
	for (int i = 0; i < sizeof(input); i++) {
		input[i] = '\0';
	}
}
void clearSingleLine(char input[24]) {
	for (int i = 0; i <= 23; i++) {
		input[i] = '\0';
	}
}

std::pair<std::string, int> getPylonAddress() {
	//std::ifstream file("ExtractorInit.txt");

	//Get system path
	char systemPath[3072];
	XPLMGetSystemPath(systemPath);
	strcat_s(systemPath, sizeof(systemPath), "Aircraft\\Extra Aircraft\\B737-800X\\plugins\\ExtractorInit.txt");
	std::ifstream file(systemPath);
	std::pair<std::string, int> returnValue;
	std::string str;

	if (!file.is_open()) {
		sprintf_s(buffer, "Error while opening file\n");
		XPLMDebugString(buffer);

		returnValue.first = "";
		returnValue.second = 0;
	}
	else if (file.bad()) {
		sprintf_s(buffer, "Error while reading file\n");
		XPLMDebugString(buffer);
		returnValue.first = "";
		returnValue.second = 0;
	}
	else {
		std::vector<std::string> stringVector;

		//Split the line
		while (std::getline(file, str))
		{
			std::string delimiter = ",";
			size_t pos = 0;
			std::string token;
			while ((pos = str.find(delimiter)) != std::string::npos) {
				token = str.substr(0, pos);
				stringVector.push_back(token);
				//printf("Token: %s|\n", token.c_str());
				str.erase(0, pos + delimiter.length());
			}
		}

		if (stringVector.size() != 0) {
			//If split succeeded
			//Make the return pair from the splited string from the text file
			returnValue.first = stringVector.at(0);
			try {
				returnValue.second = std::stoi(stringVector.at(1));
			}
			catch (std::invalid_argument& e) {
				sprintf_s(buffer, "Cannot convert port number to int\n");
				XPLMDebugString(buffer);
				returnValue.second = 0;
			}
		}
		else {
			returnValue.first = "";
			returnValue.second = 0;
		}
	}
	return returnValue;
}

void clearAll() {
	clearSingleLine(outMCDU1.pageLabel_L);
	clearSingleLine(outMCDU1.pageLabel_S);
	clearSingleLine(outMCDU1.pageLabel_I);
	clearSingleLine(outMCDU1.line1_L);
	clearSingleLine(outMCDU1.line2_L);
	clearSingleLine(outMCDU1.line3_L);
	clearSingleLine(outMCDU1.line4_L);
	clearSingleLine(outMCDU1.line5_L);
	clearSingleLine(outMCDU1.line6_L);
	clearSingleLine(outMCDU1.line1_S);
	clearSingleLine(outMCDU1.line2_S);
	clearSingleLine(outMCDU1.line3_S);
	clearSingleLine(outMCDU1.line4_S);
	clearSingleLine(outMCDU1.line5_S);
	clearSingleLine(outMCDU1.line6_S);
	clearSingleLine(outMCDU1.line1_label_S);
	clearSingleLine(outMCDU1.line2_label_S);
	clearSingleLine(outMCDU1.line3_label_S);
	clearSingleLine(outMCDU1.line4_label_S);
	clearSingleLine(outMCDU1.line5_label_S);
	clearSingleLine(outMCDU1.line6_label_S);
	clearSingleLine(outMCDU1.line1_I);
	clearSingleLine(outMCDU1.line2_I);
	clearSingleLine(outMCDU1.line3_I);
	clearSingleLine(outMCDU1.line4_I);
	clearSingleLine(outMCDU1.line5_I);
	clearSingleLine(outMCDU1.line6_I);
	clearSingleLine(outMCDU1.entry_L);
	clearSingleLine(outMCDU1.entry_I);
	clearSingleLine(outMCDU1.line1_M);
	clearSingleLine(outMCDU1.line1_G);
	clearSingleLine(outMCDU1.line2_M);
	clearSingleLine(outMCDU1.line2_G);
	clearSingleLine(outMCDU1.line3_M);
	clearSingleLine(outMCDU1.line3_G);
	clearSingleLine(outMCDU1.line4_M);
	clearSingleLine(outMCDU1.line4_G);
	clearSingleLine(outMCDU1.line5_M);
	clearSingleLine(outMCDU1.line5_G);
	clearSingleLine(outMCDU1.line6_M);
	clearSingleLine(outMCDU1.line6_G);
	clearSingleLine(outMCDU2.pageLabel_L);
	clearSingleLine(outMCDU2.pageLabel_S);
	clearSingleLine(outMCDU2.pageLabel_I);
	clearSingleLine(outMCDU2.line1_L);
	clearSingleLine(outMCDU2.line2_L);
	clearSingleLine(outMCDU2.line3_L);
	clearSingleLine(outMCDU2.line4_L);
	clearSingleLine(outMCDU2.line5_L);
	clearSingleLine(outMCDU2.line6_L);
	clearSingleLine(outMCDU2.line1_S);
	clearSingleLine(outMCDU2.line2_S);
	clearSingleLine(outMCDU2.line3_S);
	clearSingleLine(outMCDU2.line4_S);
	clearSingleLine(outMCDU2.line5_S);
	clearSingleLine(outMCDU2.line6_S);
	clearSingleLine(outMCDU2.line1_label_S);
	clearSingleLine(outMCDU2.line2_label_S);
	clearSingleLine(outMCDU2.line3_label_S);
	clearSingleLine(outMCDU2.line4_label_S);
	clearSingleLine(outMCDU2.line5_label_S);
	clearSingleLine(outMCDU2.line6_label_S);
	clearSingleLine(outMCDU2.line1_I);
	clearSingleLine(outMCDU2.line2_I);
	clearSingleLine(outMCDU2.line3_I);
	clearSingleLine(outMCDU2.line4_I);
	clearSingleLine(outMCDU2.line5_I);
	clearSingleLine(outMCDU2.line6_I);
	clearSingleLine(outMCDU2.entry_L);
	clearSingleLine(outMCDU2.entry_I);
	clearSingleLine(outMCDU2.line1_M);
	clearSingleLine(outMCDU2.line1_G);
	clearSingleLine(outMCDU2.line2_M);
	clearSingleLine(outMCDU2.line2_G);
	clearSingleLine(outMCDU2.line3_M);
	clearSingleLine(outMCDU2.line3_G);
	clearSingleLine(outMCDU2.line4_M);
	clearSingleLine(outMCDU2.line4_G);
	clearSingleLine(outMCDU2.line5_M);
	clearSingleLine(outMCDU2.line5_G);
	clearSingleLine(outMCDU2.line6_M);
	clearSingleLine(outMCDU2.line6_G);
}

static float    listenCallback(
	float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void *               inRefcon);

static float    MCDUCallback(
	float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void *               inRefcon);

PLUGIN_API int XPluginStart(
	char *      outName,
	char *      outSig,
	char *      outDesc)
{

	strcpy_s(outName, 24, "B737 Max Data Extractor");
	strcpy_s(outSig, 20, "Phuong.B737.X-Plane");
	strcpy_s(outDesc, 37, "Extract data and route from X-Plane.");

	//Initailize custom dataref
	//  Create our custom integer dataref
	debugStatus = XPLMRegisterDataAccessor("CSF/Debug", xplmType_Int, 1, GetDebugStatus, SetDebugStatus,
		NULL, NULL,
		NULL, NULL,
		NULL, NULL,
		NULL, NULL,
		NULL, NULL,
		NULL, NULL);

	// Find and intialize custom dataref
	debugStatus = XPLMFindDataRef("CSF/Debug");
	XPLMSetDatai(debugStatus, 1);

	//-----------------------------------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		sprintf_s(buffer, "WSAStartup for UDP failed with error %d\n", iResult);
		XPLMDebugString(buffer);
	}

	//Create dispatch socket
	if ((dispatchSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		sprintf_s(buffer, "dispatchSocket create failed with error code : %d\n", WSAGetLastError());
		XPLMDebugString(buffer);
		//exit(EXIT_FAILURE);
	}

	std::pair<std::string, int> pylon = getPylonAddress();
	if (pylon.first != "" && pylon.second != 0) {
		sprintf_s(buffer, "Got pylon address: %s:%d\n", pylon.first.c_str(), pylon.second);
		XPLMDebugString(buffer);

		pylonAddress.sin_family = AF_INET;
		pylonAddress.sin_port = htons(pylon.second);
		InetPton(AF_INET, _T(pylon.first.c_str()), &pylonAddress.sin_addr.S_un.S_addr);

	}
	else {
		sprintf_s(buffer, "Cannot get pylon address. Use default value.\n");
		XPLMDebugString(buffer);

		//Use default address
		pylonAddress.sin_family = AF_INET;
		pylonAddress.sin_port = htons(8989);
		InetPton(AF_INET, _T("127.0.0.1"), &pylonAddress.sin_addr.S_un.S_addr);
	}
	//Set address structure for the route data
	MCDU1.sin_family = AF_INET;
	MCDU1.sin_port = htons(MCDUPort);
	InetPton(AF_INET, _T(SERVER1), &MCDU1.sin_addr.S_un.S_addr);

	//Set address structure for the route data
	MCDU2.sin_family = AF_INET;
	MCDU2.sin_port = htons(MCDUPort);
	InetPton(AF_INET, _T(SERVER2), &MCDU2.sin_addr.S_un.S_addr);
	

	//Find primary datarefs
	Airspeed = XPLMFindDataRef("sim/flightmodel/position/indicated_airspeed");
	trueAirspeed = XPLMFindDataRef("sim/cockpit2/gauges/indicators/true_airspeed_kts_pilot");
	SelectedAirspeed = XPLMFindDataRef("sim/cockpit/autopilot/airspeed");
	GroundSpeed = XPLMFindDataRef("laminar/b738/fmodpack/real_groundspeed");
	Pitch = XPLMFindDataRef("sim/flightmodel/position/theta");
	Roll = XPLMFindDataRef("sim/flightmodel/position/phi");
	VerticalSpeed = XPLMFindDataRef("sim/flightmodel/position/vh_ind_fpm");
	VerticalSpeedBug = XPLMFindDataRef("sim/cockpit2/autopilot/vvi_dial_fpm");
	backupDisplayAltitude = XPLMFindDataRef("laminar/B738/gauges/standby_altitude_ft");
	altitudeAboveGround = XPLMFindDataRef("sim/flightmodel/position/y_agl");
	SelectedAltitude = XPLMFindDataRef("sim/cockpit/autopilot/altitude");
	Heading = XPLMFindDataRef("sim/flightmodel/position/mag_psi");
	trueHeading = XPLMFindDataRef("sim/flightmodel/position/true_psi");
	SelectedHeading = XPLMFindDataRef("laminar/B738/autopilot/mcp_hdg_dial");
	Latitude = XPLMFindDataRef("sim/flightmodel/position/latitude");
	Longitude = XPLMFindDataRef("sim/flightmodel/position/longitude");
	fuelLeftTank = XPLMFindDataRef("laminar/B738/fuel/left_tank_kgs");
	fuelRightTank = XPLMFindDataRef("laminar/B738/fuel/right_tank_kgs");
	fuelCenterTank = XPLMFindDataRef("laminar/B738/fuel/center_tank_kgs");
	N1Left = XPLMFindDataRef("laminar/B738/engine/indicators/N1_percent_1");
	N1Right = XPLMFindDataRef("laminar/B738/engine/indicators/N1_percent_2");
	N2Left = XPLMFindDataRef("laminar/B738/engine/indicators/N2_percent_1");
	N2Right = XPLMFindDataRef("laminar/B738/engine/indicators/N2_percent_2");
	EGTLeft = XPLMFindDataRef("laminar/B738/engine/eng1_egt");
	EGTRight = XPLMFindDataRef("laminar/B738/engine/eng2_egt");
	OilPressureLeft = XPLMFindDataRef("laminar/B738/engine/eng1_oil_press");
	OilPressureRight = XPLMFindDataRef("laminar/B738/engine/eng2_oil_press");
	OilTemp = XPLMFindDataRef("sim/flightmodel/engine/ENGN_oil_temp_c");
	OilQuantity = XPLMFindDataRef("sim/flightmodel/engine/ENGN_oil_quan");
	FlapsHandle = XPLMFindDataRef("sim/cockpit2/controls/flap_handle_deploy_ratio");
	WindDirection = XPLMFindDataRef("sim/cockpit2/gauges/indicators/wind_heading_deg_mag");
	Windspeed = XPLMFindDataRef("sim/cockpit2/gauges/indicators/wind_speed_kts");
	FuelFlow = XPLMFindDataRef("sim/cockpit2/engine/indicators/fuel_flow_kg_sec");
	GreenArcStatus = XPLMFindDataRef("laminar/B738/EFIS/green_arc_show");
	GreenArcValue = XPLMFindDataRef("laminar/B738/EFIS/green_arc");
	GreenArcDistance = XPLMFindDataRef("CSF/arc_dist");
	xtrack = XPLMFindDataRef("laminar/B738/fms/xtrack");
	RadioAltimeter = XPLMFindDataRef("sim/cockpit2/gauges/indicators/radio_altimeter_height_ft_pilot");
	RadioAltimeterStep = XPLMFindDataRef("sim/aircraft/autopilot/radio_altimeter_step_ft");
	VNAV_ERR = XPLMFindDataRef("laminar/B738/fms/vnav_err_pfd");
	RunwayAltitude = XPLMFindDataRef("laminar/B738/pfd/rwy_altitude");
	waypointAltitude = XPLMFindDataRef("laminar/B738/fms/legs_alt_rest1");
	zuluTime_Hour = XPLMFindDataRef("sim/cockpit2/clock_timer/zulu_time_hours");
	zuluTime_Minute = XPLMFindDataRef("sim/cockpit2/clock_timer/zulu_time_minutes");
	zuluTime_Second = XPLMFindDataRef("sim/cockpit2/clock_timer/zulu_time_seconds");
	flapUpValue = XPLMFindDataRef("laminar/B738/pfd/flaps_up");
	flapUpStatus = XPLMFindDataRef("laminar/B738/pfd/flaps_up_show");
	flap1Value = XPLMFindDataRef("laminar/B738/pfd/flaps_1");
	flap1Status = XPLMFindDataRef("laminar/B738/pfd/flaps_1_show");
	flap5Value = XPLMFindDataRef("laminar/B738/pfd/flaps_5");
	flap5Status = XPLMFindDataRef("laminar/B738/pfd/flaps_5_show");
	flap15Value = XPLMFindDataRef("laminar/B738/pfd/flaps_15");
	flap15Status = XPLMFindDataRef("laminar/B738/pfd/flaps_15_show");
	flap25Value = XPLMFindDataRef("laminar/B738/pfd/flaps_25");
	flap25Status = XPLMFindDataRef("laminar/B738/pfd/flaps_25_show");
	maxSpeed = XPLMFindDataRef("laminar/B738/pfd/max_speed");
	minSpeed = XPLMFindDataRef("laminar/B738/pfd/min_speed");
	minSpeedStatus = XPLMFindDataRef("laminar/B738/pfd/min_speed_show");
	VORLineStatus = XPLMFindDataRef("laminar/B738/pfd/vor1_line_show");
	VORLineCourse = XPLMFindDataRef("laminar/B738/pfd/vor1_sel_rotate");
	Course = XPLMFindDataRef("laminar/B738/autopilot/course_pilot");
	VOR1_Show = XPLMFindDataRef("laminar/B738/pfd/vor1_show");
	VORName1 = XPLMFindDataRef("laminar/B738/pfd/vor1_sel_id");
	VORName2 = XPLMFindDataRef("laminar/B738/pfd/vor2_sel_id");
	VORDistance1 = XPLMFindDataRef("sim/cockpit/radios/nav1_dme_dist_m");
	VORDistance2 = XPLMFindDataRef("sim/cockpit/radios/nav2_dme_dist_m");
	runwayStartLat = XPLMFindDataRef("laminar/B738/fms/dest_runway_start_lat");
	runwayStartLon = XPLMFindDataRef("laminar/B738/fms/dest_runway_start_lon");
	runwayEndLat = XPLMFindDataRef("laminar/B738/fms/dest_runway_end_lat");
	runwayEndLon = XPLMFindDataRef("laminar/B738/fms/dest_runway_end_lon");
	runwaycourse = XPLMFindDataRef("laminar/B738/fms/dest_runway_crs");
	HdefPath = XPLMFindDataRef("sim/cockpit/radios/nav1_hdef_dot");
	mapModePilot = XPLMFindDataRef("laminar/B738/EFIS_control/capt/map_mode_pos");
	mapModeCoPilot = XPLMFindDataRef("laminar/B738/EFIS_control/fo/map_mode_pos");
	NAVBearing = XPLMFindDataRef("laminar/radios/pilot/nav_bearing");
	DecisionHeight = XPLMFindDataRef("laminar/B738/pfd/dh_pilot");
	DecisionHeightStatus = XPLMFindDataRef("laminar/B738/EFIS_control/cpt/minimums_pfd");
	PullUpStatus = XPLMFindDataRef("laminar/b738/alert/pfd_pull_up");
	Acceleration = XPLMFindDataRef("sim/cockpit2/gauges/indicators/airspeed_acceleration_kts_sec_pilot");
	ILS_Horizontal_Value = XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_hdef_dots_pilot");
	ILS_Vertical_Value = XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_vdef_dots_pilot");
	ILS_Show = XPLMFindDataRef("laminar/B738/pfd/ils_show");
	ILS_Status = XPLMFindDataRef("sim/cockpit2/autopilot/glideslope_status");
	VREFSpeed = XPLMFindDataRef("laminar/B738/FMS/vref");
	VREFStatus = XPLMFindDataRef("laminar/B738/pfd/vspeed_vref_show");
	onGround = XPLMFindDataRef("sim/flightmodel/failures/onground_any");
	scenario = XPLMFindDataRef("CSF/Scenario");
	holdHeading1 = XPLMFindDataRef("CSF/hold_crs1");
	holdHeading2 = XPLMFindDataRef("CSF/hold_crs2");
	holdLegLength = XPLMFindDataRef("CSF/hold_lenght");
	holdRadius = XPLMFindDataRef("CSF/hold_radius");
	holdStatus = XPLMFindDataRef("laminar/B738/nd/hold_type");
	RNP = XPLMFindDataRef("laminar/B738/fms/rnp");
	ANP = XPLMFindDataRef("laminar/B738/fms/anp");
	V1Speed = XPLMFindDataRef("laminar/B738/FMS/v1");
	V2Speed = XPLMFindDataRef("laminar/B738/FMS/v2");
	VRSpeed = XPLMFindDataRef("laminar/B738/FMS/vr");
	Mach = XPLMFindDataRef("sim/flightmodel/misc/machno");
	Fix = XPLMFindDataRef("laminar/B738/nd/fix_id00");
	airspeedIsMach = XPLMFindDataRef("sim/cockpit/autopilot/airspeed_is_mach");
	legs_alt_rest_type = XPLMFindDataRef("laminar/B738/fms/legs_alt_rest_type");
	TOD = XPLMFindDataRef("laminar/B738/fms/td_dist");
	throttleLevel1 = XPLMFindDataRef("laminar/B738/engine/eng1_N1_bug");
	throttleLevel2 = XPLMFindDataRef("laminar/B738/engine/eng2_N1_bug");
	//Data that needs to be either pilot or copilot
	//Pilot Side
	AltitudePilot = XPLMFindDataRef("sim/cockpit2/gauges/indicators/altitude_ft_pilot");
	mapRangePilot = XPLMFindDataRef("laminar/B738/EFIS/capt/map_range");
	airportStatusPilot = XPLMFindDataRef("laminar/B738/EFIS/EFIS_airport_on");
	waypointStatusPilot = XPLMFindDataRef("laminar/B738/EFIS/EFIS_fix_on");
	vor1StatusPilot = XPLMFindDataRef("laminar/B738/EFIS_control/capt/vor1_off_pfd");
	vor2StatusPilot = XPLMFindDataRef("laminar/B738/EFIS_control/capt/vor2_off_pfd");
	baroSTDStatusPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_set_std_pilot");
	baroInHgPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_sel_in_hg_pilot");
	baroUnitStatusPilot = XPLMFindDataRef("laminar/B738/EFIS_control/capt/baro_in_hpa_pfd");
	navaidStatusPilot = XPLMFindDataRef("laminar/B738/EFIS/EFIS_vor_on");
	pfdModePilot = XPLMFindDataRef("laminar/B738/autopilot/pfd_mode");
	vertPathPilot = XPLMFindDataRef("laminar/B738/pfd/pfd_vert_path");
	Text_Line1Pilot = XPLMFindDataRef("laminar/B738/pfd/cpt_nav_txt1");
	Text_Line2Pilot = XPLMFindDataRef("laminar/B738/pfd/cpt_nav_txt2");
	BaroBoxPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_box_pilot_show");
	//Co Pilot Side
	AltitudeCoPilot = XPLMFindDataRef("sim/cockpit2/gauges/indicators/altitude_ft_copilot");
	mapRangeCoPilot = XPLMFindDataRef("laminar/B738/EFIS/fo/map_range");
	airportStatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS/fo/EFIS_airport_on");
	waypointStatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS/fo/EFIS_fix_on");
	vor1StatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS_control/fo/vor1_off_pfd");
	vor2StatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS_control/fo/vor2_off_pfd");
	baroSTDStatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_set_std_copilot");
	baroInHgCoPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_sel_in_hg_copilot");
	baroUnitStatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS_control/fo/baro_in_hpa_fo");
	navaidStatusCoPilot = XPLMFindDataRef("laminar/B738/EFIS/fo/EFIS_vor_on");
	pfdModeCoPilot = XPLMFindDataRef("laminar/B738/autopilot/pfd_mode_fo");
	vertPathCoPilot = XPLMFindDataRef("laminar/B738/pfd/pfd_vert_path_fo");
	Text_Line1CoPilot = XPLMFindDataRef("laminar/B738/pfd/fo_nav_txt1");
	Text_Line2CoPilot = XPLMFindDataRef("laminar/B738/pfd/fo_nav_txt2");
	BaroBoxCoPilot = XPLMFindDataRef("laminar/B738/EFIS/baro_box_copilot_show");
	//Find route datarefs
	legNumber = XPLMFindDataRef("laminar/B738/vnav/legs_num");
	waypointLatitudeArray = XPLMFindDataRef("laminar/B738/fms/legs_lat");
	waypointLongitudeArray = XPLMFindDataRef("laminar/B738/fms/legs_lon");
	legNumber2 = XPLMFindDataRef("laminar/B738/fms/legs_num2");
	waypointLatitudeArray2 = XPLMFindDataRef("laminar/B738/fms/legs_lat_2");
	waypointLongitudeArray2 = XPLMFindDataRef("laminar/B738/fms/legs_lon_2");
	ETA = XPLMFindDataRef("laminar/B738/fms/id_eta");
	waypointName = XPLMFindDataRef("laminar/B738/fms/fpln_nav_id");
	waypointDistance = XPLMFindDataRef("laminar/B738/fms/lnav_dist_next");
	legName = XPLMFindDataRef("laminar/B738/fms/legs");
	legName2 = XPLMFindDataRef("laminar/B738/fms/legs_2");
	legModActive = XPLMFindDataRef("laminar/B738/fms/legs_mod_active");
	FlightDirectorStatus = XPLMFindDataRef("laminar/B738/autopilot/flight_director_pos");
	FlightDirectorPitch = XPLMFindDataRef("laminar/B738/pfd/flight_director_pitch_pilot");
	FlightDirectorRoll = XPLMFindDataRef("sim/cockpit2/autopilot/flight_director_roll_deg");
	wptType_0 = XPLMFindDataRef("laminar/B738/nd/wpt_type00");
	wptType_1 = XPLMFindDataRef("laminar/B738/nd/wpt_type01");
	wptType_2 = XPLMFindDataRef("laminar/B738/nd/wpt_type02");
	wptType_3 = XPLMFindDataRef("laminar/B738/nd/wpt_type03");
	wptType_4 = XPLMFindDataRef("laminar/B738/nd/wpt_type04");
	wptType_5 = XPLMFindDataRef("laminar/B738/nd/wpt_type05");
	wptType_6 = XPLMFindDataRef("laminar/B738/nd/wpt_type06");
	wptType_7 = XPLMFindDataRef("laminar/B738/nd/wpt_type07");
	wptType_8 = XPLMFindDataRef("laminar/B738/nd/wpt_type08");
	wptType_9 = XPLMFindDataRef("laminar/B738/nd/wpt_type09");
	wptType_10 = XPLMFindDataRef("laminar/B738/nd/wpt_type10");
	wptType_11 = XPLMFindDataRef("laminar/B738/nd/wpt_type11");
	wptType_12 = XPLMFindDataRef("laminar/B738/nd/wpt_type12");
	wptType_13 = XPLMFindDataRef("laminar/B738/nd/wpt_type13");
	wptType_14 = XPLMFindDataRef("laminar/B738/nd/wpt_type14");
	wptType_15 = XPLMFindDataRef("laminar/B738/nd/wpt_type15");
	wptType_16 = XPLMFindDataRef("laminar/B738/nd/wpt_type16");
	wptType_17 = XPLMFindDataRef("laminar/B738/nd/wpt_type17");
	wptType_18 = XPLMFindDataRef("laminar/B738/nd/wpt_type18");
	wptType_19 = XPLMFindDataRef("laminar/B738/nd/wpt_type19");
	wptNameWhite_0 = XPLMFindDataRef("laminar/B738/nd/wpt_id00w");
	wptNameWhite_1 = XPLMFindDataRef("laminar/B738/nd/wpt_id01w");
	wptNameWhite_2 = XPLMFindDataRef("laminar/B738/nd/wpt_id02w");
	wptNameWhite_3 = XPLMFindDataRef("laminar/B738/nd/wpt_id03w");
	wptNameWhite_4 = XPLMFindDataRef("laminar/B738/nd/wpt_id04w");
	wptNameWhite_5 = XPLMFindDataRef("laminar/B738/nd/wpt_id05w");
	wptNameWhite_6 = XPLMFindDataRef("laminar/B738/nd/wpt_id06w");
	wptNameWhite_7 = XPLMFindDataRef("laminar/B738/nd/wpt_id07w");
	wptNameWhite_8 = XPLMFindDataRef("laminar/B738/nd/wpt_id08w");
	wptNameWhite_9 = XPLMFindDataRef("laminar/B738/nd/wpt_id09w");
	wptNameWhite_10 = XPLMFindDataRef("laminar/B738/nd/wpt_id10w");
	wptNameWhite_11 = XPLMFindDataRef("laminar/B738/nd/wpt_id11w");
	wptNameWhite_12 = XPLMFindDataRef("laminar/B738/nd/wpt_id12w");
	wptNameWhite_13 = XPLMFindDataRef("laminar/B738/nd/wpt_id13w");
	wptNameWhite_14 = XPLMFindDataRef("laminar/B738/nd/wpt_id14w");
	wptNameWhite_15 = XPLMFindDataRef("laminar/B738/nd/wpt_id15w");
	wptNameWhite_16 = XPLMFindDataRef("laminar/B738/nd/wpt_id16w");
	wptNameWhite_17 = XPLMFindDataRef("laminar/B738/nd/wpt_id17w");
	wptNameWhite_18 = XPLMFindDataRef("laminar/B738/nd/wpt_id18w");
	wptNameWhite_19 = XPLMFindDataRef("laminar/B738/nd/wpt_id19w");
	wptNameMagenta_0 = XPLMFindDataRef("laminar/B738/nd/wpt_id00m");
	wptNameMagenta_1 = XPLMFindDataRef("laminar/B738/nd/wpt_id01m");
	wptNameMagenta_2 = XPLMFindDataRef("laminar/B738/nd/wpt_id02m");
	wptNameMagenta_3 = XPLMFindDataRef("laminar/B738/nd/wpt_id03m");
	wptNameMagenta_4 = XPLMFindDataRef("laminar/B738/nd/wpt_id04m");
	wptNameMagenta_5 = XPLMFindDataRef("laminar/B738/nd/wpt_id05m");
	wptNameMagenta_6 = XPLMFindDataRef("laminar/B738/nd/wpt_id06m");
	wptNameMagenta_7 = XPLMFindDataRef("laminar/B738/nd/wpt_id07m");
	wptNameMagenta_8 = XPLMFindDataRef("laminar/B738/nd/wpt_id08m");
	wptNameMagenta_9 = XPLMFindDataRef("laminar/B738/nd/wpt_id09m");
	wptNameMagenta_10 = XPLMFindDataRef("laminar/B738/nd/wpt_id10m");
	wptNameMagenta_11 = XPLMFindDataRef("laminar/B738/nd/wpt_id11m");
	wptNameMagenta_12 = XPLMFindDataRef("laminar/B738/nd/wpt_id12m");
	wptNameMagenta_13 = XPLMFindDataRef("laminar/B738/nd/wpt_id13m");
	wptNameMagenta_14 = XPLMFindDataRef("laminar/B738/nd/wpt_id14m");
	wptNameMagenta_15 = XPLMFindDataRef("laminar/B738/nd/wpt_id15m");
	wptNameMagenta_16 = XPLMFindDataRef("laminar/B738/nd/wpt_id16m");
	wptNameMagenta_17 = XPLMFindDataRef("laminar/B738/nd/wpt_id17m");
	wptNameMagenta_18 = XPLMFindDataRef("laminar/B738/nd/wpt_id18m");
	wptNameMagenta_19 = XPLMFindDataRef("laminar/B738/nd/wpt_id19m");
	//Find auto pilot datarefs
	autoThrottleStatus = XPLMFindDataRef("laminar/B738/autopilot/autothrottle_status");
	autoPilotSpeedMode = XPLMFindDataRef("laminar/B738/autopilot/pfd_spd_mode");
	autoPilotHeadingMode = XPLMFindDataRef("laminar/B738/autopilot/pfd_hdg_mode");
	autopilotAltitudeMode = XPLMFindDataRef("laminar/B738/autopilot/pfd_alt_mode");
	autoPilotHeadingModeArm = XPLMFindDataRef("laminar/B738/autopilot/pfd_hdg_mode_arm");
	autoPilotAltitudeModeArm = XPLMFindDataRef("laminar/B738/autopilot/pfd_alt_mode_arm");
	CMDAStatus = XPLMFindDataRef("laminar/B738/autopilot/cmd_a_status");
	CMDBStatus = XPLMFindDataRef("laminar/B738/autopilot/cmd_b_status");
	pilotPage = XPLMFindDataRef("CSF/pilotUsingDataComm");
	copilotPage = XPLMFindDataRef("CSF/copilotUsingDataComm");
	//Find MCDU datarefs
	pageLabel_L = XPLMFindDataRef("laminar/B738/fmc1/Line00_L");
	pageLabel_I = XPLMFindDataRef("laminar/B738/fmc1/Line00_I");
	pageLabel_S = XPLMFindDataRef("laminar/B738/fmc1/Line00_S");
	line1_L = XPLMFindDataRef("laminar/B738/fmc1/Line01_L");
	line2_L = XPLMFindDataRef("laminar/B738/fmc1/Line02_L");
	line3_L = XPLMFindDataRef("laminar/B738/fmc1/Line03_L");
	line4_L = XPLMFindDataRef("laminar/B738/fmc1/Line04_L");
	line5_L = XPLMFindDataRef("laminar/B738/fmc1/Line05_L");
	line6_L = XPLMFindDataRef("laminar/B738/fmc1/Line06_L");
	line1_S = XPLMFindDataRef("laminar/B738/fmc1/Line01_X");
	line2_S = XPLMFindDataRef("laminar/B738/fmc1/Line02_X");
	line3_S = XPLMFindDataRef("laminar/B738/fmc1/Line03_X");
	line4_S = XPLMFindDataRef("laminar/B738/fmc1/Line04_X");
	line5_S = XPLMFindDataRef("laminar/B738/fmc1/Line05_X");
	line6_S = XPLMFindDataRef("laminar/B738/fmc1/Line06_X");
	line1_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line01_S");
	line2_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line02_S");
	line3_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line03_S");
	line4_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line04_S");
	line5_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line05_S");
	line6_label_S = XPLMFindDataRef("laminar/B738/fmc1/Line06_S");
	line1_I = XPLMFindDataRef("laminar/B738/fmc1/Line01_I");
	line2_I = XPLMFindDataRef("laminar/B738/fmc1/Line02_I");
	line3_I = XPLMFindDataRef("laminar/B738/fmc1/Line03_I");
	line4_I = XPLMFindDataRef("laminar/B738/fmc1/Line04_I");
	line5_I = XPLMFindDataRef("laminar/B738/fmc1/Line05_I");
	line6_I = XPLMFindDataRef("laminar/B738/fmc1/Line06_I");
	entry_L = XPLMFindDataRef("laminar/B738/fmc1/Line_entry");
	entry_I = XPLMFindDataRef("laminar/B738/fmc1/Line_entry_I");
	line1_M = XPLMFindDataRef("laminar/B738/fmc1/Line01_M");
	line2_M = XPLMFindDataRef("laminar/B738/fmc1/Line02_M");
	line3_M = XPLMFindDataRef("laminar/B738/fmc1/Line03_M");
	line4_M = XPLMFindDataRef("laminar/B738/fmc1/Line04_M");
	line5_M = XPLMFindDataRef("laminar/B738/fmc1/Line05_M");
	line6_M = XPLMFindDataRef("laminar/B738/fmc1/Line06_M");
	line1_G = XPLMFindDataRef("laminar/B738/fmc1/Line01_G");
	line2_G = XPLMFindDataRef("laminar/B738/fmc1/Line02_G");
	line3_G = XPLMFindDataRef("laminar/B738/fmc1/Line03_G");
	line4_G = XPLMFindDataRef("laminar/B738/fmc1/Line04_G");
	line5_G = XPLMFindDataRef("laminar/B738/fmc1/Line05_G");
	line6_G = XPLMFindDataRef("laminar/B738/fmc1/Line06_G");
	//Find MCDU datarefs
	pageLabel_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line00_L");
	pageLabel_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line00_I");
	pageLabel_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line00_S");
	line1_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_L");
	line2_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_L");
	line3_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_L");
	line4_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_L");
	line5_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_L");
	line6_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_L");
	line1_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_X");
	line2_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_X");
	line3_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_X");
	line4_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_X");
	line5_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_X");
	line6_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_X");
	line1_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_S");
	line2_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_S");
	line3_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_S");
	line4_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_S");
	line5_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_S");
	line6_label_S_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_S");
	line1_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_I");
	line2_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_I");
	line3_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_I");
	line4_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_I");
	line5_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_I");
	line6_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_I");
	entry_L_ = XPLMFindDataRef("laminar/B738/fmc2/Line_entry");
	entry_I_ = XPLMFindDataRef("laminar/B738/fmc2/Line_entry_I");
	line1_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_M");
	line2_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_M");
	line3_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_M");
	line4_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_M");
	line5_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_M");
	line6_M_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_M");
	line1_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line01_G");
	line2_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line02_G");
	line3_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line03_G");
	line4_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line04_G");
	line5_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line05_G");
	line6_G_ = XPLMFindDataRef("laminar/B738/fmc2/Line06_G");

	XPLMRegisterFlightLoopCallback(listenCallback, -1.0, NULL);
	sprintf_s(buffer, "Finished register primary callback.\n");
	XPLMDebugString(buffer);

	XPLMRegisterFlightLoopCallback(MCDUCallback, -1.0, NULL);
	sprintf_s(buffer, "Finished register MCDU callback.\n");
	XPLMDebugString(buffer);

	return 1;
}

void prepareWaypointName(primaryData outProtobuf) {
	//char waypointNameWithType[512];
	std::string waypointNameWithType;
	char tempWPTName[512] = "";
	if (outProtobuf.waypointtype(0) != 0) {
		if (outProtobuf.waypointtype(0) == 1 || outProtobuf.waypointtype(0) == 3) {
			XPLMGetDatab(wptNameWhite_0, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";
			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(0) == 2 || outProtobuf.waypointtype(0) == 4) {
			XPLMGetDatab(wptNameMagenta_0, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(1) != 0) {
		if (outProtobuf.waypointtype(1) == 1 || outProtobuf.waypointtype(1) == 3) {
			XPLMGetDatab(wptNameWhite_1, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(1) == 2 || outProtobuf.waypointtype(1) == 4) {
			XPLMGetDatab(wptNameMagenta_1, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(2) != 0) {
		if (outProtobuf.waypointtype(2) == 1 || outProtobuf.waypointtype(2) == 3) {
			XPLMGetDatab(wptNameWhite_2, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(2) == 2 || outProtobuf.waypointtype(2) == 4) {
			XPLMGetDatab(wptNameMagenta_2, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(3) != 0) {
		if (outProtobuf.waypointtype(3) == 1 || outProtobuf.waypointtype(3) == 3) {
			XPLMGetDatab(wptNameWhite_3, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(3) == 2 || outProtobuf.waypointtype(3) == 4) {
			XPLMGetDatab(wptNameMagenta_3, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(4) != 0) {
		if (outProtobuf.waypointtype(4) == 1 || outProtobuf.waypointtype(4) == 3) {
			XPLMGetDatab(wptNameWhite_4, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(4) == 2 || outProtobuf.waypointtype(4) == 4) {
			XPLMGetDatab(wptNameMagenta_4, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(5) != 0) {
		if (outProtobuf.waypointtype(5) == 1 || outProtobuf.waypointtype(5) == 3) {
			XPLMGetDatab(wptNameWhite_5, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(5) == 2 || outProtobuf.waypointtype(5) == 4) {
			XPLMGetDatab(wptNameMagenta_5, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(6) != 0) {
		if (outProtobuf.waypointtype(6) == 1 || outProtobuf.waypointtype(6) == 3) {
			XPLMGetDatab(wptNameWhite_6, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(6) == 2 || outProtobuf.waypointtype(6) == 4) {
			XPLMGetDatab(wptNameMagenta_6, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(7) != 0) {
		if (outProtobuf.waypointtype(7) == 1 || outProtobuf.waypointtype(7) == 3) {
			XPLMGetDatab(wptNameWhite_7, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(7) == 2 || outProtobuf.waypointtype(7) == 4) {
			XPLMGetDatab(wptNameMagenta_7, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(8) != 0) {
		if (outProtobuf.waypointtype(8) == 1 || outProtobuf.waypointtype(8) == 3) {
			XPLMGetDatab(wptNameWhite_8, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(8) == 2 || outProtobuf.waypointtype(8) == 4) {
			XPLMGetDatab(wptNameMagenta_8, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(9) != 0) {
		if (outProtobuf.waypointtype(9) == 1 || outProtobuf.waypointtype(9) == 3) {
			XPLMGetDatab(wptNameWhite_9, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(9) == 2 || outProtobuf.waypointtype(9) == 4) {
			XPLMGetDatab(wptNameMagenta_9, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(10) != 0) {
		if (outProtobuf.waypointtype(10) == 1 || outProtobuf.waypointtype(10) == 3) {
			XPLMGetDatab(wptNameWhite_10, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(10) == 2 || outProtobuf.waypointtype(10) == 4) {
			XPLMGetDatab(wptNameMagenta_10, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(11) != 0) {
		if (outProtobuf.waypointtype(11) == 1 || outProtobuf.waypointtype(11) == 3) {
			XPLMGetDatab(wptNameWhite_11, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(11) == 2 || outProtobuf.waypointtype(11) == 4) {
			XPLMGetDatab(wptNameMagenta_11, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(12) != 0) {
		if (outProtobuf.waypointtype(12) == 1 || outProtobuf.waypointtype(12) == 3) {
			XPLMGetDatab(wptNameWhite_12, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(12) == 2 || outProtobuf.waypointtype(12) == 4) {
			XPLMGetDatab(wptNameMagenta_12, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(13) != 0) {
		if (outProtobuf.waypointtype(13) == 1 || outProtobuf.waypointtype(13) == 3) {
			XPLMGetDatab(wptNameWhite_13, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(13) == 2 || outProtobuf.waypointtype(13) == 4) {
			XPLMGetDatab(wptNameMagenta_13, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(14) != 0) {
		if (outProtobuf.waypointtype(14) == 1 || outProtobuf.waypointtype(14) == 3) {
			XPLMGetDatab(wptNameWhite_14, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(14) == 2 || outProtobuf.waypointtype(14) == 4) {
			XPLMGetDatab(wptNameMagenta_14, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(15) != 0) {
		if (outProtobuf.waypointtype(15) == 1 || outProtobuf.waypointtype(15) == 3) {
			XPLMGetDatab(wptNameWhite_15, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(15) == 2 || outProtobuf.waypointtype(15) == 4) {
			XPLMGetDatab(wptNameMagenta_15, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(16) != 0) {
		if (outProtobuf.waypointtype(16) == 1 || outProtobuf.waypointtype(16) == 3) {
			XPLMGetDatab(wptNameWhite_0, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(16) == 2 || outProtobuf.waypointtype(16) == 4) {
			XPLMGetDatab(wptNameMagenta_16, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(17) != 0) {
		if (outProtobuf.waypointtype(17) == 1 || outProtobuf.waypointtype(17) == 3) {
			XPLMGetDatab(wptNameWhite_17, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(17) == 2 || outProtobuf.waypointtype(17) == 4) {
			XPLMGetDatab(wptNameMagenta_17, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(18) != 0) {
		if (outProtobuf.waypointtype(18) == 1 || outProtobuf.waypointtype(18) == 3) {
			XPLMGetDatab(wptNameWhite_18, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(18) == 2 || outProtobuf.waypointtype(18) == 4) {
			XPLMGetDatab(wptNameMagenta_18, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}

	if (outProtobuf.waypointtype(19) != 0) {
		if (outProtobuf.waypointtype(19) == 1 || outProtobuf.waypointtype(19) == 3) {
			XPLMGetDatab(wptNameWhite_19, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
		else if (outProtobuf.waypointtype(19) == 2 || outProtobuf.waypointtype(19) == 4) {
			XPLMGetDatab(wptNameMagenta_19, &tempWPTName, 0, 512);
			std::string temp(tempWPTName);
			memset(tempWPTName, 0, sizeof(tempWPTName));

			temp += " ";

			//strcat_s(waypointNameWithType, sizeof(waypointNameWithType), temp.c_str());
			waypointNameWithType += temp;
		}
	}
	outProtobuf.set_waypointnamewithtype(waypointNameWithType);
}
PLUGIN_API void XPluginStop(void)
{
	//Unregister custom dataref
	XPLMUnregisterDataAccessor(debugStatus);
	//Close the socket when plugin stops
	closesocket(dispatchSocket);
	WSACleanup();
	/* Unregister the callback */
	XPLMUnregisterFlightLoopCallback(listenCallback, NULL);
	XPLMUnregisterFlightLoopCallback(MCDUCallback, NULL);
} 

PLUGIN_API void XPluginDisable(void) { }
PLUGIN_API int  XPluginEnable(void) {
	return 1;
}
PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMsg, void * inParam) { }

float listenCallback(float inElapsedSinceLastCall,
	float inElapsedTimeSinceLastFlightLoop,
	int inCounter,
	void* inRefcon)
{

	int bufferSize = -1;
	
	outProtobuf.set_airspeed(XPLMGetDataf(Airspeed));
	outProtobuf.set_trueairspeed(XPLMGetDataf(trueAirspeed));
	outProtobuf.set_selectedairspeed(XPLMGetDataf(SelectedAirspeed));
	outProtobuf.set_groundspeed(XPLMGetDataf(GroundSpeed));
	outProtobuf.set_pitch(XPLMGetDataf(Pitch));
	outProtobuf.set_roll(XPLMGetDataf(Roll));
	outProtobuf.set_verticalspeed(XPLMGetDataf(VerticalSpeed));
	outProtobuf.set_verticalspeedbug(XPLMGetDataf(VerticalSpeedBug));
	outProtobuf.set_altitudepilot(XPLMGetDataf(AltitudePilot));
	outProtobuf.set_altitudecopilot(XPLMGetDataf(AltitudeCoPilot));
	outProtobuf.set_backupdisplayaltitude(XPLMGetDataf(backupDisplayAltitude));
	outProtobuf.set_altitudeaboveground(XPLMGetDataf(altitudeAboveGround));
	outProtobuf.set_selectedaltitude(XPLMGetDataf(SelectedAltitude));
	outProtobuf.set_heading(XPLMGetDataf(Heading));
	outProtobuf.set_trueheading(XPLMGetDataf(trueHeading));
	outProtobuf.set_selectedheading(XPLMGetDataf(SelectedHeading));
	outProtobuf.set_latitude(XPLMGetDataf(Latitude));
	outProtobuf.set_longitude(XPLMGetDataf(Longitude));
	outProtobuf.set_fuellefttank(XPLMGetDataf(fuelLeftTank));
	outProtobuf.set_fuelrighttank(XPLMGetDataf(fuelRightTank));
	outProtobuf.set_fuelcentertank(XPLMGetDataf(fuelCenterTank));
	outProtobuf.set_n1left(XPLMGetDataf(N1Left));
	outProtobuf.set_n1right(XPLMGetDataf(N1Right));
	outProtobuf.set_n2left(XPLMGetDataf(N2Left));
	outProtobuf.set_n2right(XPLMGetDataf(N2Right));
	outProtobuf.set_egtleft(XPLMGetDataf(EGTLeft));
	outProtobuf.set_egtright(XPLMGetDataf(EGTRight));
	XPLMGetDatavf(OilTemp, OilTempOut, 0, 2);
	outProtobuf.set_oiltempleft(OilTempOut[0]);
	outProtobuf.set_oiltempright(OilTempOut[1]);
	outProtobuf.set_oilpressureleft(XPLMGetDataf(OilPressureLeft));
	outProtobuf.set_oilpressureright(XPLMGetDataf(OilPressureRight));
	XPLMGetDatavf(OilQuantity, OilQuantityOut, 0, 2);
	outProtobuf.set_oilquantityleft(OilQuantityOut[0]);
	outProtobuf.set_oilquantityright(OilQuantityOut[1]);
	outProtobuf.set_flapshandle(XPLMGetDataf(FlapsHandle));
	outProtobuf.set_winddirection(XPLMGetDataf(WindDirection));
	outProtobuf.set_windspeed(XPLMGetDataf(Windspeed));
	outProtobuf.set_flightdirectorstatus(XPLMGetDatai(FlightDirectorStatus));
	outProtobuf.set_flightdirectorpitch(XPLMGetDataf(FlightDirectorPitch));
	outProtobuf.set_flightdirectorroll(XPLMGetDataf(FlightDirectorRoll));
	XPLMGetDatavf(FuelFlow, FuelFlowOut, 0, 2);
	outProtobuf.set_fuelflowleft(FuelFlowOut[0]);
	outProtobuf.set_fuelflowright(FuelFlowOut[1]);
	outProtobuf.set_greenarcstatus(XPLMGetDatai(GreenArcStatus));
	outProtobuf.set_greenarcvalue(XPLMGetDataf(GreenArcValue));
	outProtobuf.set_greenarcdistance(XPLMGetDataf(GreenArcDistance));
	outProtobuf.set_xtrack(XPLMGetDataf(xtrack));
	outProtobuf.set_radioaltimeter(XPLMGetDataf(RadioAltimeter));
	outProtobuf.set_radioaltimeterstep(XPLMGetDataf(RadioAltimeterStep));
	outProtobuf.set_vnav_err(XPLMGetDataf(VNAV_ERR));
	outProtobuf.set_runwayaltitude(XPLMGetDataf(RunwayAltitude));
	outProtobuf.add_zulutime(XPLMGetDatai(zuluTime_Hour));
	outProtobuf.add_zulutime(XPLMGetDatai(zuluTime_Minute));
	outProtobuf.add_zulutime(XPLMGetDatai(zuluTime_Second));
	outProtobuf.set_flapupvalue(XPLMGetDatai(flapUpValue));
	outProtobuf.set_flapupstatus(XPLMGetDatai(flapUpStatus));
	outProtobuf.set_flap1value(XPLMGetDatai(flap1Value));
	outProtobuf.set_flap1status(XPLMGetDatai(flap1Status));
	outProtobuf.set_flap5value(XPLMGetDatai(flap5Value));
	outProtobuf.set_flap5status(XPLMGetDatai(flap5Status));
	outProtobuf.set_flap15value(XPLMGetDatai(flap15Value));
	outProtobuf.set_flap15status(XPLMGetDatai(flap15Status));
	outProtobuf.set_flap25value(XPLMGetDatai(flap25Value));
	outProtobuf.set_flap25status(XPLMGetDatai(flap25Status));
	outProtobuf.set_maxspeed(XPLMGetDataf(maxSpeed));
	outProtobuf.set_minspeed(XPLMGetDataf(minSpeed));
	outProtobuf.set_minspeedstatus(XPLMGetDatai(minSpeedStatus));
	outProtobuf.set_vorlinestatus(XPLMGetDatai(VORLineStatus));
	outProtobuf.set_vorlinecourse(XPLMGetDataf(VORLineCourse));
	outProtobuf.set_course(XPLMGetDataf(Course));
	outProtobuf.set_vor1_show(XPLMGetDatai(VOR1_Show));

	bufferSize = XPLMGetDatab(VORName1, NULL, 0, 0);
	char tempVORname1[24];
	if (XPLMGetDatab(VORName1, &tempVORname1, 1, bufferSize)) {
		tempVORname1[bufferSize] = '\0';
		std::string stempVORname1(tempVORname1);
		outProtobuf.set_vorname1(stempVORname1);
	}

	bufferSize = XPLMGetDatab(VORName2, NULL, 0, 0);
	char tempVORname2[24];
	if (XPLMGetDatab(VORName2, &tempVORname2, 1, bufferSize)) {
		tempVORname2[bufferSize] = '\0';
		std::string stempVORname2(tempVORname2);
		outProtobuf.set_vorname2(stempVORname2);
	}
	outProtobuf.set_vordistance1(XPLMGetDataf(VORDistance1));
	outProtobuf.set_vordistance2(XPLMGetDataf(VORDistance2));
	outProtobuf.set_runwaycourse(XPLMGetDataf(runwaycourse));
	outProtobuf.add_runwaystart(XPLMGetDataf(runwayStartLat));
	outProtobuf.add_runwaystart(XPLMGetDataf(runwayStartLon));
	outProtobuf.add_runwayend(XPLMGetDataf(runwayEndLat));
	outProtobuf.add_runwayend(XPLMGetDataf(runwayEndLon));
	outProtobuf.set_hdefpath(XPLMGetDataf(HdefPath));
	outProtobuf.set_navbearing(XPLMGetDataf(NAVBearing));
	outProtobuf.set_decisionheight(XPLMGetDataf(DecisionHeight));
	outProtobuf.set_decisionheightstatus(XPLMGetDataf(DecisionHeightStatus));
	outProtobuf.set_pullupstatus(XPLMGetDatai(PullUpStatus));
	outProtobuf.set_acceleration(XPLMGetDataf(Acceleration));
	outProtobuf.set_ils_horizontal_value(XPLMGetDataf(ILS_Horizontal_Value));
	outProtobuf.set_ils_vertical_value(XPLMGetDataf(ILS_Vertical_Value));
	outProtobuf.set_ils_show(XPLMGetDatai(ILS_Show));
	outProtobuf.set_ils_status(XPLMGetDatai(ILS_Status));
	outProtobuf.set_vrefspeed(XPLMGetDataf(VREFSpeed));
	outProtobuf.set_vrefstatus(XPLMGetDatai(VREFStatus));
	outProtobuf.set_onground(XPLMGetDatai(onGround));
	char tempScenario[5];
	bufferSize = XPLMGetDatab(scenario, NULL, 0, 0);
	if (XPLMGetDatab(scenario, &tempScenario, 0, bufferSize)) {
		tempScenario[bufferSize] = '\0';
		std::string stempScenario(tempScenario);
		outProtobuf.set_scenario(tempScenario);
	}
	outProtobuf.set_holdheading1(XPLMGetDataf(holdHeading1));
	outProtobuf.set_holdheading2(XPLMGetDataf(holdHeading2));
	outProtobuf.set_holdleglength(XPLMGetDataf(holdLegLength));
	outProtobuf.set_holdradius(XPLMGetDataf(holdRadius));
	int tempHoldStatus;
	XPLMGetDatavi(holdStatus, &tempHoldStatus, 0, 1);
	outProtobuf.set_holdstatus(tempHoldStatus);
	outProtobuf.set_rnp(XPLMGetDataf(RNP));
	outProtobuf.set_anp(XPLMGetDataf(ANP));
	outProtobuf.set_v1speed(XPLMGetDataf(V1Speed));
	outProtobuf.set_v2speed(XPLMGetDataf(V2Speed));
	outProtobuf.set_vrspeed(XPLMGetDataf(VRSpeed));
	outProtobuf.set_mach(XPLMGetDataf(Mach));
	char tempFixName[5];
	bufferSize = XPLMGetDatab(Fix, NULL, 0, 0);
	if (XPLMGetDatab(Fix, &tempFixName, 0, bufferSize)) {
		tempFixName[bufferSize] = '\0';
		std::string stempFixName(tempFixName);
		outProtobuf.set_fix(stempFixName);
	}
	outProtobuf.set_airspeedismach(XPLMGetDatai(airspeedIsMach));
	int temp_legs_alt_rest_type[128];
	XPLMGetDatavi(legs_alt_rest_type, temp_legs_alt_rest_type, 0, 128);
	for (int i = 0; i < 128; i++) {
		outProtobuf.add_legs_alt_rest_type(temp_legs_alt_rest_type[i]);
	}
	outProtobuf.set_tod(XPLMGetDataf(TOD));
	outProtobuf.set_throttlelevel1(XPLMGetDataf(throttleLevel1));
	outProtobuf.set_throttlelevel2(XPLMGetDataf(throttleLevel2));


	//Data that needs to be either pilot or copilot
	//Pilot Side
	outProtobuf.set_maprangepilot(XPLMGetDatai(mapRangePilot));
	outProtobuf.set_airportstatuspilot(XPLMGetDatai(airportStatusPilot));
	outProtobuf.set_waypointstatuspilot(XPLMGetDatai(waypointStatusPilot));
	outProtobuf.set_vor1statuspilot(XPLMGetDatai(vor1StatusPilot));
	outProtobuf.set_vor2statuspilot(XPLMGetDatai(vor2StatusPilot));
	outProtobuf.set_barostdstatuspilot(XPLMGetDatai(baroSTDStatusPilot));
	outProtobuf.set_baroinhgpilot(XPLMGetDataf(baroInHgPilot));
	outProtobuf.set_barounitstatuspilot(XPLMGetDatai(baroUnitStatusPilot));
	outProtobuf.set_mapmodepilot(XPLMGetDatai(mapModePilot));
	outProtobuf.set_navaidstatuspilot(XPLMGetDatai(navaidStatusPilot));
	outProtobuf.set_pfdmodepilot(XPLMGetDatai(pfdModePilot));
	outProtobuf.set_vertpathpilot(XPLMGetDatai(vertPathPilot));
	char Temp_Text_Line1Pilot[10];
	char Temp_Text_Line2Pilot[10];
	bufferSize = XPLMGetDatab(Text_Line1Pilot, NULL, 0, 0);
	if (XPLMGetDatab(Text_Line1Pilot, &Temp_Text_Line1Pilot, 0, bufferSize)) {
		Temp_Text_Line1Pilot[bufferSize] = '\0';
		std::string sTemp_Text_Line1Pilot(Temp_Text_Line1Pilot);
		outProtobuf.set_text_line1pilot(sTemp_Text_Line1Pilot);
	}
	bufferSize = XPLMGetDatab(Text_Line2Pilot, NULL, 0, 0);
	if (XPLMGetDatab(Text_Line2Pilot, &Temp_Text_Line2Pilot, 0, bufferSize)) {
		Temp_Text_Line2Pilot[bufferSize] = '\0';
		std::string sTemp_Text_Line2Pilot(Temp_Text_Line2Pilot);
		outProtobuf.set_text_line1pilot(sTemp_Text_Line2Pilot);
	}
	outProtobuf.set_baroboxpilot(XPLMGetDatai(BaroBoxPilot));

	//CoPilot Side
	outProtobuf.set_maprangecopilot(XPLMGetDatai(mapRangeCoPilot));
	outProtobuf.set_airportstatuscopilot(XPLMGetDatai(airportStatusCoPilot));
	outProtobuf.set_waypointstatuscopilot(XPLMGetDatai(waypointStatusCoPilot));
	outProtobuf.set_vor1statuscopilot(XPLMGetDatai(vor1StatusCoPilot));
	outProtobuf.set_vor2statuscopilot(XPLMGetDatai(vor2StatusCoPilot));
	outProtobuf.set_barostdstatuscopilot(XPLMGetDatai(baroSTDStatusCoPilot));
	outProtobuf.set_baroinhgcopilot(XPLMGetDataf(baroInHgCoPilot));
	outProtobuf.set_barounitstatuscopilot(XPLMGetDatai(baroUnitStatusCoPilot));
	outProtobuf.set_mapmodecopilot(XPLMGetDatai(mapModeCoPilot));
	outProtobuf.set_navaidstatuscopilot(XPLMGetDatai(navaidStatusCoPilot));
	outProtobuf.set_pfdmodecopilot(XPLMGetDatai(pfdModeCoPilot));
	outProtobuf.set_vertpathcopilot(XPLMGetDatai(vertPathCoPilot));
	char Temp_Text_Line1CoPilot[10];
	char Temp_Text_Line2CoPilot[10];
	bufferSize = XPLMGetDatab(Text_Line1CoPilot, NULL, 0, 0);
	if (XPLMGetDatab(Text_Line1CoPilot, &Temp_Text_Line1CoPilot, 0, bufferSize)) {
		Temp_Text_Line1CoPilot[bufferSize] = '\0';
		std::string sTemp_Text_Line1CoPilot(Temp_Text_Line1CoPilot);
		outProtobuf.set_text_line1copilot(sTemp_Text_Line1CoPilot);
	}
	bufferSize = XPLMGetDatab(Text_Line2CoPilot, NULL, 0, 0);
	if (XPLMGetDatab(Text_Line2CoPilot, &Temp_Text_Line2CoPilot, 0, bufferSize)) {
		Temp_Text_Line2CoPilot[bufferSize] = '\0';
		std::string sTemp_Text_Line2CoPilot(Temp_Text_Line2CoPilot);
		outProtobuf.set_text_line2copilot(sTemp_Text_Line2CoPilot);
	}
	outProtobuf.set_baroboxcopilot(XPLMGetDatai(BaroBoxCoPilot));

	//Route data
	char waypointName[25];
	bufferSize = XPLMGetDatab(waypointName, NULL, 0, 0);
	if (XPLMGetDatab(waypointName, &waypointName, 0, bufferSize)) {
		waypointName[bufferSize] = '\0';
		std::string swaypointName(waypointName);
		outProtobuf.set_waypointname(swaypointName);
	}
	outProtobuf.set_legnumber(XPLMGetDatai(legNumber));
	outProtobuf.set_legnumber2(XPLMGetDatai(legNumber2));
	int waypointAltitude[128];
	XPLMGetDatavi(waypointAltitude, waypointAltitude, 0, outProtobuf.legnumber());
	for (int i = 0; i < outProtobuf.legnumber(); i++)
	{
		outProtobuf.add_waypointaltitude(waypointAltitude[i]);
	}
	outProtobuf.set_waypointdistance(XPLMGetDataf(waypointDistance));
	outProtobuf.set_legmodactive(XPLMGetDatai(legModActive));

	//Get ETA and convert it to float
	if (XPLMGetDatab(ETA, &buffer, 0, 24) > 0) {
		outProtobuf.set_eta(std::stof(buffer));
	}

	//Get legs name
	char tempLegName[513];
	char tempLegName2[513];
	bufferSize = XPLMGetDatab(legName, NULL, 0, 0);
	if (XPLMGetDatab(legName, &tempLegName, 0, bufferSize)) {
		tempLegName[bufferSize] = '\0';
		std::string stempLegName(tempLegName);
		outProtobuf.set_legname(stempLegName);
	}
	bufferSize = XPLMGetDatab(legName2, NULL, 0, 0);
	if (XPLMGetDatab(legName2, &tempLegName2, 0, bufferSize)) {
		tempLegName2[bufferSize] = '\0';
		std::string stempLegName2(tempLegName2);
		outProtobuf.set_legname2(stempLegName2);
	}
	float temp_latitudeArray[128];
	float temp_longitudeArray[128];
	float temp_latitudeArray2[128];
	float temp_longitudeArray2[128];
	XPLMGetDatavf(waypointLatitudeArray, temp_latitudeArray, 0, outProtobuf.legnumber());
	XPLMGetDatavf(waypointLongitudeArray, temp_longitudeArray, 0, outProtobuf.legnumber());
	XPLMGetDatavf(waypointLatitudeArray2, temp_latitudeArray2, 0, outProtobuf.legnumber2());
	XPLMGetDatavf(waypointLongitudeArray2, temp_longitudeArray2, 0, outProtobuf.legnumber2());

	for (int i = 0; i < outProtobuf.legnumber(); i++)
	{
		outProtobuf.add_latitudearray(temp_latitudeArray[i]);
		outProtobuf.add_longitudearray(temp_longitudeArray[i]);
	}

	for (int i = 0; i < outProtobuf.legnumber2(); i++)
	{
		outProtobuf.add_latitudearray2(temp_latitudeArray2[i]);
		outProtobuf.add_longitudearray2(temp_longitudeArray2[i]);
	}

	//Get waypoint type
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_0));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_1));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_2));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_3));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_4));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_5));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_6));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_7));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_8));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_9));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_10));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_11));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_12));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_13));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_14));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_15));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_16));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_17));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_18));
	outProtobuf.add_waypointtype(XPLMGetDatai(wptType_19));

	//Get the name of the waypoint that has the type != 0
	prepareWaypointName(outProtobuf);
	//Get auto pilot
	outProtobuf.set_autothrottlestatus(XPLMGetDatai(autoThrottleStatus));
	outProtobuf.set_autopilotspeedmode(XPLMGetDatai(autoPilotSpeedMode));
	outProtobuf.set_autopilotheadingmode(XPLMGetDatai(autoPilotHeadingMode));
	outProtobuf.set_autopilotaltitudemode(XPLMGetDatai(autopilotAltitudeMode));
	outProtobuf.set_autopilotheadingmodearm(XPLMGetDatai(autoPilotHeadingModeArm));
	outProtobuf.set_autopilotaltitudemodearm(XPLMGetDatai(autoPilotAltitudeModeArm));
	outProtobuf.set_cmdastatus(XPLMGetDatai(CMDAStatus));
	outProtobuf.set_cmdbstatus(XPLMGetDatai(CMDBStatus));

	int protobufSize = outProtobuf.ByteSize();
	sprintf_s(buffer, "Message size: %d\n", protobufSize);
	XPLMDebugString(buffer);

	char * sendData = new char[protobufSize];
	outProtobuf.SerializeToArray(sendData, protobufSize);
	if (sendto(dispatchSocket, sendData, protobufSize, 0, (struct sockaddr *) &pylonAddress, sizeof(pylonAddress)) == SOCKET_ERROR)
	{
		sprintf_s(buffer, "Sent to Pylon error : %d\n", WSAGetLastError());
		XPLMDebugString(buffer);
	}
	else {
		//reset protobuf message;
		outProtobuf.Clear();
	}

	return -1.0;
}

float MCDUCallback(float inElapsedSinceLastCall,
	float inElapsedTimeSinceLastFlightLoop,
	int inCounter,
	void* inRefcon)
{

	/*outMCDU1.page = XPLMGetDatai(pilotPage);
	//Get page label
	XPLMGetDatab(pageLabel_L, &outMCDU1.pageLabel_L, 0, 24);
	XPLMGetDatab(pageLabel_S, &outMCDU1.pageLabel_S, 0, 24);
	XPLMGetDatab(pageLabel_I, &outMCDU1.pageLabel_I, 0, 24);

	//Get large lines
	XPLMGetDatab(line1_L, &outMCDU1.line1_L, 0, 24);
	XPLMGetDatab(line2_L, &outMCDU1.line2_L, 0, 24);
	XPLMGetDatab(line3_L, &outMCDU1.line3_L, 0, 24);
	XPLMGetDatab(line4_L, &outMCDU1.line4_L, 0, 24);
	XPLMGetDatab(line5_L, &outMCDU1.line5_L, 0, 24);
	XPLMGetDatab(line6_L, &outMCDU1.line6_L, 0, 24);

	//Get small lines
	XPLMGetDatab(line1_S, &outMCDU1.line1_S, 0, 24);
	XPLMGetDatab(line2_S, &outMCDU1.line2_S, 0, 24);
	XPLMGetDatab(line3_S, &outMCDU1.line3_S, 0, 24);
	XPLMGetDatab(line4_S, &outMCDU1.line4_S, 0, 24);
	XPLMGetDatab(line5_S, &outMCDU1.line5_S, 0, 24);
	XPLMGetDatab(line6_S, &outMCDU1.line6_S, 0, 24);

	//Get lable lines
	XPLMGetDatab(line1_label_S, &outMCDU1.line1_label_S, 0, 24);
	XPLMGetDatab(line2_label_S, &outMCDU1.line2_label_S, 0, 24);
	XPLMGetDatab(line3_label_S, &outMCDU1.line3_label_S, 0, 24);
	XPLMGetDatab(line4_label_S, &outMCDU1.line4_label_S, 0, 24);
	XPLMGetDatab(line5_label_S, &outMCDU1.line5_label_S, 0, 24);
	XPLMGetDatab(line6_label_S, &outMCDU1.line6_label_S, 0, 24);

	//Get magenta lines
	XPLMGetDatab(line1_M, &outMCDU1.line1_M, 0, 24);
	XPLMGetDatab(line2_M, &outMCDU1.line2_M, 0, 24);
	XPLMGetDatab(line3_M, &outMCDU1.line3_M, 0, 24);
	XPLMGetDatab(line4_M, &outMCDU1.line4_M, 0, 24);
	XPLMGetDatab(line5_M, &outMCDU1.line5_M, 0, 24);
	XPLMGetDatab(line6_M, &outMCDU1.line6_M, 0, 24);

	//Get green lines
	XPLMGetDatab(line1_G, &outMCDU1.line1_G, 0, 24);
	XPLMGetDatab(line2_G, &outMCDU1.line2_G, 0, 24);
	XPLMGetDatab(line3_G, &outMCDU1.line3_G, 0, 24);
	XPLMGetDatab(line4_G, &outMCDU1.line4_G, 0, 24);
	XPLMGetDatab(line5_G, &outMCDU1.line5_G, 0, 24);
	XPLMGetDatab(line6_G, &outMCDU1.line6_G, 0, 24);

	//Get entry line
	XPLMGetDatab(entry_L, &outMCDU1.entry_L, 0, 24);


	outMCDU2.page = XPLMGetDatai(copilotPage);
	//Get page label
	XPLMGetDatab(pageLabel_L_, &outMCDU2.pageLabel_L, 0, 24);
	XPLMGetDatab(pageLabel_S_, &outMCDU2.pageLabel_S, 0, 24);
	XPLMGetDatab(pageLabel_I_, &outMCDU2.pageLabel_I, 0, 24);

	//Get large lines
	XPLMGetDatab(line1_L_, &outMCDU2.line1_L, 0, 24);
	XPLMGetDatab(line2_L_, &outMCDU2.line2_L, 0, 24);
	XPLMGetDatab(line3_L_, &outMCDU2.line3_L, 0, 24);
	XPLMGetDatab(line4_L_, &outMCDU2.line4_L, 0, 24);
	XPLMGetDatab(line5_L_, &outMCDU2.line5_L, 0, 24);
	XPLMGetDatab(line6_L_, &outMCDU2.line6_L, 0, 24);

	//Get small lines
	XPLMGetDatab(line1_S_, &outMCDU2.line1_S, 0, 24);
	XPLMGetDatab(line2_S_, &outMCDU2.line2_S, 0, 24);
	XPLMGetDatab(line3_S_, &outMCDU2.line3_S, 0, 24);
	XPLMGetDatab(line4_S_, &outMCDU2.line4_S, 0, 24);
	XPLMGetDatab(line5_S_, &outMCDU2.line5_S, 0, 24);
	XPLMGetDatab(line6_S_, &outMCDU2.line6_S, 0, 24);

	//Get lable lines
	XPLMGetDatab(line1_label_S_, &outMCDU2.line1_label_S, 0, 24);
	XPLMGetDatab(line2_label_S_, &outMCDU2.line2_label_S, 0, 24);
	XPLMGetDatab(line3_label_S_, &outMCDU2.line3_label_S, 0, 24);
	XPLMGetDatab(line4_label_S_, &outMCDU2.line4_label_S, 0, 24);
	XPLMGetDatab(line5_label_S_, &outMCDU2.line5_label_S, 0, 24);
	XPLMGetDatab(line6_label_S_, &outMCDU2.line6_label_S, 0, 24);

	//Get magenta lines
	XPLMGetDatab(line1_M_, &outMCDU2.line1_M, 0, 24);
	XPLMGetDatab(line2_M_, &outMCDU2.line2_M, 0, 24);
	XPLMGetDatab(line3_M_, &outMCDU2.line3_M, 0, 24);
	XPLMGetDatab(line4_M_, &outMCDU2.line4_M, 0, 24);
	XPLMGetDatab(line5_M_, &outMCDU2.line5_M, 0, 24);
	XPLMGetDatab(line6_M_, &outMCDU2.line6_M, 0, 24);

	//Get green lines
	XPLMGetDatab(line1_G_, &outMCDU2.line1_G, 0, 24);
	XPLMGetDatab(line2_G_, &outMCDU2.line2_G, 0, 24);
	XPLMGetDatab(line3_G_, &outMCDU2.line3_G, 0, 24);
	XPLMGetDatab(line4_G_, &outMCDU2.line4_G, 0, 24);
	XPLMGetDatab(line5_G_, &outMCDU2.line5_G, 0, 24);
	XPLMGetDatab(line6_G_, &outMCDU2.line6_G, 0, 24);

	//Get entry line
	XPLMGetDatab(entry_L_, &outMCDU2.entry_L, 0, 24);

	//Send to GUI
	if (sendto(dispatchSocket, (char*)&outMCDU1, sizeof(outMCDU1), 0, (struct sockaddr *) &MCDU1, MCDU1_len) == SOCKET_ERROR)
	{
		sprintf_s(buffer, "Sent to MCDU 1 error : %d\n", WSAGetLastError());
		XPLMDebugString(buffer);
	}
	//Send to GUI
	if (sendto(dispatchSocket, (char*)&outMCDU2, sizeof(outMCDU2), 0, (struct sockaddr *) &MCDU2, MCDU2_len) == SOCKET_ERROR)
	{
		sprintf_s(buffer, "Sent to MCDU 2 error : %d\n", WSAGetLastError());
		XPLMDebugString(buffer);
	}

	//Clear all previous line
	clearAll();*/
	return -1.0;
}